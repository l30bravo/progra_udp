#include <iostream>
using namespace std;



int main(){
	float valor_comercial, costo_danos, devolucion=0;
	const float p_seguro=0.25;
	const float p_devolucion=0.7;

	cout<<"VALOR COMERCIAL DEL AUTO:";
	cin >> valor_comercial;

	cout <<"COSTO DE LOS DAÑOS:";
	cin >> costo_danos;

	if (costo_danos >= (valor_comercial*p_seguro)) {
		devolucion = costo_danos * p_devolucion;
	}

	cout.precision(20); // digitos a mostrar 
	cout <<"El seguro le devolvera: $"<<devolucion<<endl;

	return 0;
}
