#include <iostream>
#include <vector>
using namespace std;

int num_columnas=5;
int num_registros=3;


bool validaDocumentos(string pCirulacion, string soa, string revTecnica){
  if (pCirulacion == "false"){
    return false;
  }

  if (soa == "false"){
    return false;
  }

  if (revTecnica == "false"){
    return false;
  }

  return true;
}


int main() {
  string database[num_registros][num_columnas];


  //inica db
  for (int i=0; i<num_registros; i++){
    for (int j=0; j<num_columnas;j++){
      database[i][j]=" ";
    }
  }

  //INGRESAR AUTOS
  for (int j=0; j<num_registros;j++){
        cout<<"Ingrese patente "<<j<<": "<<endl;
        cin>> database[j][0];

        cout<<"Ingrese dueno "<<j<<": "<<endl;
        cin>> database[j][1];

        cout<<"¿Tiene el permiso de circulacion al dia? [true || false]"<<j<<": "<<endl;
        cin>> database[j][2];

        cout<<"¿Tiene el permiso de SOA al dia? [true || false]"<<j<<": "<<endl;
        cin>> database[j][3];

        cout<<"¿Tiene el permiso de Rev Tecnica al dia? [true || false]"<<j<<": "<<endl;
        cin>> database[j][4];
  }

  //Imprimir
    for (int i=0; i< num_registros; i++){
    cout<<endl;
    for (int j=0; j<num_columnas; j++){
      if (j > 0){
        cout<<",";
      }
      cout<<database[i][j];
    }
  }
  cout<<endl;
  cout<<"BASE DE DATOS COMPLETADA  [OK]"<<endl;
  
  //Buscar un auto
  string patente;
  bool exit = false;
  while (!exit){
    cout<<"Ingrese la patente de un auto a buscar - [exit] para salir:";
    cin>>patente;

    if (patente=="exit"){
      exit = true;
      break;
    }

    cout<<"Buscando patente..."<<endl;
    
    for (int j=0; j<num_registros; j++){
        if (database[j][0] == patente){
            bool quitarAuto = validaDocumentos(database[j][2], database[j][3], database[j][4]);
            if (!quitarAuto){
              cout<<"El auto no cumple con los papeles al dia"<<endl;
              break;
            }else{
              cout<<"El auto esta al dia! :D"<<endl;
              break;
            }
        }

        if ((j+1) == num_registros){
          cout<<"No se encontro vehiculo"<<endl;
        }
      }
  }
 


  return 0;
}


