#include <iostream>
#include <vector>
#include "string"
using namespace std;

string decodeUDP(string msj){
  string msjDecode = "";
  for (int i=0; i<msj.size(); i++){
    int pos = int(msj[i]);
    pos = pos - 3;
    char newChar = char(pos);
    msjDecode += newChar;
  }
  return msjDecode;
}

string codeUDP(string msj){
  string msjCode = "";
  for (int i=0; i<msj.size(); i++){
    int pos = int(msj[i]);
    pos = pos + 3;
    char newChar = char(pos);
    msjCode += newChar;
  }
  return msjCode;
}

int main() {
  string msj = "";
  cout<<"Ingrese mensaje:";
  cin >> msj;

  //CODIFICAR
  string msjCode = codeUDP(msj);
  cout<<"Mensaje encodeado: |"<<msjCode<<"|"<<endl;

  string msjOriginal = decodeUDP(msjCode);
  cout<<"Mensaje decodificado: |"<<msjOriginal<<"|"<<endl;

  return 0;
}


