#include <iostream>
using namespace std;

int NUMVAGONES = 8;


void llenarTren( int trenPasajeros[]);
void imprimirTren(int trenPasajeros[]);
int sumarPasajeros(int trenPasajeros[]);

int main() {
  int trenPasajeros[NUMVAGONES];
  llenarTren(trenPasajeros);
  imprimirTren(trenPasajeros);
  int totalPasajeros = sumarPasajeros(trenPasajeros);
  cout<<"El tren lleva: "<<totalPasajeros<<" pasajeros"<<endl;
  return 0;
}


void llenarTren( int trenPasajeros[]){
  for (int i=0; i<NUMVAGONES; i++){
    cout<<"Ingrese numero de Pasajeros para el vagon "<<i<<" :";
    cin>>trenPasajeros[i];
  }
}

void imprimirTren(int trenPasajeros[]){
 for (int i=0; i<NUMVAGONES; i++){
    if (i>0){
      cout<<"|";
    }
    cout<<trenPasajeros[i];
  }
  cout<<endl;
}


int sumarPasajeros(int trenPasajeros[]){
  int suma =0;
  for (int i=0; i<NUMVAGONES; i++){
    suma+= trenPasajeros[i];
  }
  return suma;
}

