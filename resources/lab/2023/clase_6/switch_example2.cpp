#include <iostream>
#include <string>
using namespace std;

int main(){
  string day = "";
  cout<<"Ingrese el numero del dia"<<endl;
  cin>>day;
  switch (day) {
    case "lunes":
      cout << "Monday"<<endl;
      break;
    case 2:
      cout << "Tuesday"<<endl;
      break;
    case 3:
      cout << "Wednesday"<<endl;
      break;
    case 4:
      cout << "Thursday"<<endl;
      break;
    case 5:
      cout << "Friday"<<endl;
      break;
    case 6:
      cout << "Saturday"<<endl;
      break;
    case 7:
      cout << "Sunday"<<endl;
      break;
    default:
      cout<<"No existe ese dia en este planeta"<<endl;
  }
  return 0;
}

