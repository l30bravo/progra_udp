#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

int random_int(int min, int max){
   return min + rand() % (max+1 - min);
}


int main(){
	string opt = "yes";
	int lukyNumber =0;
	cout<<"LUCKY NUMBERS 2023:"<<endl;
	cout<<"==================="<<endl;

	while (opt == "yes"){
		srand( time( NULL ) );
		lukyNumber = random_int(0, 31);
		cout<<"Your Lucky Number is: "<<lukyNumber<<endl;
		cout<<"Do you want to continue? (yes or no): ";
		cin >> opt;
		if (opt !="yes" && opt != "no"){
			cout<<"Invalid option!"<<endl;
		}
	}

	cout<<"Bye!"<<endl;
	return 0;
}
