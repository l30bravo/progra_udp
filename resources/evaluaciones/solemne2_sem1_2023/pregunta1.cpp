#include <iostream>
using namespace std;

void votar(int voto, int &a,int &b, int &c, int &d, int &nulosblancos);
void estadisticasFinales(int a, int b,int c,int d, int nulosblancos);
bool estaPresente(string candidatos, string candidato);

int main ( )
{
   int a =0, b=0, c=0, d=0, nulosblancos=0,votosmesa, voto;
   cout<<"INgrese votos de la mesa"<<endl;
   cin>> votosmesa;
   for (int i=1; i<= votosmesa; i++){
      cout<<"Ingrese opcion voto "<<i<<endl;
      cout<<"1 - candidato a"<<endl;
      cout<<"2 - candidato b"<<endl;
      cout<<"3 - candidato c"<<endl;
      cout<<"4 - candidato d"<<endl;
      cout<<"5 - candidato nulo o blanco"<<endl;
      do {
        cin>>voto;
      } while(voto <1 || voto > 5);
      votar(voto, a,b,c,d,nulosblancos);

      cout<<"Recuento Parcial"<<endl;
      cout<<"Votos a: "<<a<<endl;
      cout<<"Votos b: "<<b<<endl;
      cout<<"Votos c: "<<c<<endl;
      cout<<"Votos d: "<<d<<endl;
      cout<<"Votos nulos o blancos: "<<nulosblancos<<endl;
   }

   cout<<"Resultado final"<<endl;
   estadisticasFinales(a,b,c,d, nulosblancos);

}


void votar(int voto, int &a,int &b, int &c, int &d, int &nulosblancos){
  if (voto == 1){
    a++;
  }
  if (voto == 2){
    b++;
  }
  if (voto == 3){
    c++;
  }
  if (voto == 4){
    d++;
  }
  if (voto == 5){
    nulosblancos++;
  }
}


void estadisticasFinales(int a, int b,int c,int d, int nulosblancos){
  string nombreGandor="a";
  float porcentageGandor = (a*1.0/(a+b+c+d+nulosblancos)*100);

  if (porcentageGandor < (b*1.0/(a+b+c+d+nulosblancos)*100)) {
    porcentageGandor = (b*1.0/(a+b+c+d+nulosblancos)*100);
    nombreGandor = "b";
  }

  if (porcentageGandor < (c*1.0/(a+b+c+d+nulosblancos)*100)) {
    porcentageGandor = (c*1.0/(a+b+c+d+nulosblancos)*100);
    nombreGandor = "c";
  }

  if (porcentageGandor < (d*1.0/(a+b+c+d+nulosblancos)*100)) {
    porcentageGandor = (d*1.0/(a+b+c+d+nulosblancos)*100);
    nombreGandor = "d";
  }

  if (porcentageGandor < (nulosblancos*1.0/(a+b+c+d+nulosblancos)*100)) {
    porcentageGandor = (nulosblancos*1.0/(a+b+c+d+nulosblancos)*100);
    nombreGandor = "nulos o blancos";
  }

  //Empates
  if (porcentageGandor == (a*1.0/(a+b+c+d+nulosblancos)*100)){
      if (!estaPresente(nombreGandor, "a")){
        nombreGandor += ", a";
      }
     
  }

  if (porcentageGandor == (b*1.0/(a+b+c+d+nulosblancos)*100)){
     if (!estaPresente(nombreGandor, "b")){
      nombreGandor += ", b";
     }
  }

   if (porcentageGandor == (c*1.0/(a+b+c+d+nulosblancos)*100)){
     if (!estaPresente(nombreGandor, "c")){
      nombreGandor += ", c";
     }
  }

 if (porcentageGandor == (d*1.0/(a+b+c+d+nulosblancos)*100)){
   if (!estaPresente(nombreGandor, "d")){
      nombreGandor += ", d";
   }
  }

  if (porcentageGandor == (nulosblancos*1.0/(a+b+c+d+nulosblancos)*100)){
     if (!estaPresente(nombreGandor, "nulos")){
      nombreGandor += ", nulos o blancos";
     }
  }

  cout<<"Candidato(s) "<<nombreGandor<<" lidera con un "<<porcentageGandor<<"%"<<endl;
}

bool estaPresente(string candidatos, string candidato){
  int pos = candidatos.find(candidato);
  //cout<<"CANDIDATOS:"<<candidatos<<", candidato:"<<candidato<<", POS: "<<pos<<endl;
  if ( pos >= 0){
    return true;
  }
  return false;
}