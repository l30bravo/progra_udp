#include <iostream>
using namespace std;

int NUM_FACULTADES=20;

void asignarAlunosPorFacultad(int alumnos[ ]);
void resultadoAsignacion(int montoDistribuir, int base, int alumnos[ ]);

int main ( ){
  int alumnos[NUM_FACULTADES];
  int montoDistribuir, base =0;
  cout<<"Ingrese monto a distribuir: "<<endl;
  cin >> montoDistribuir;
  cout<<"Ingrese monto base por alumno: "<<endl;
  cin >> base;
  asignarAlunosPorFacultad(alumnos);
  resultadoAsignacion(montoDistribuir, base, alumnos);
}

void asignarAlunosPorFacultad(int alumnos[ ]){
  for(int i=0; i<NUM_FACULTADES; i++){
    cout<<"Ingrese el numero de alumnos para la facultad "<<i<<":"<<endl;
    cin>>alumnos[i];
  }
}

void resultadoAsignacion(int montoDistribuir, int base, int alumnos[ ]){
  int montoPorFacultad = montoDistribuir / NUM_FACULTADES;
  int facuDeficit=0;
  int facuSinDeficit=0;
  for (int i=0; i<NUM_FACULTADES; i++){
    float valor = montoPorFacultad *1.0 / alumnos[i];
    //cout<<"Valor: "<<valor<<" vs Base:"<<base<<endl;
    if ( valor < base){
      facuDeficit++;
    }else{
      facuSinDeficit++;
    }
  }

  cout<<"Monto asignado por facultad: "<<montoPorFacultad<<endl;
  cout<<"Cantidad de Facultades con deficit: "<<facuDeficit<<endl;
  cout<<"Cantidad de Facutlades sin deficit: "<<facuSinDeficit<<endl;
}